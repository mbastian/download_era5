import cdsapi
from pathlib import Path
from tqdm import tqdm
import os
import tarfile

PATH_OUT = 'data_oras5'
ls_varnames = [
        'sea_surface_temperature',
        'meridional_velocity', 
        'potential_temperature', 
        'zonal_velocity'
        ]

ls_3d_vars = [
        'meridional_velocity', 
        'potential_temperature', 
        'zonal_velocity'
        ]


c = cdsapi.Client()

os.makedirs(PATH_OUT, exist_ok=True)
os.makedirs(Path(PATH_OUT, 'nc/'), exist_ok=True)

for year in tqdm(range(1958, 2021)):
    for varname in ls_varnames:
        if varname in ls_3d_vars:
            vert_res = 'all_levels'
        else:
            vert_res = 'single_level'
        path_out = Path(PATH_OUT, f'{varname}-{year}-{vert_res}.tar.gz')
        if os.path.isfile(path_out):
            continue
        try:
            c.retrieve(
                'reanalysis-oras5',
                {
                    'format': 'tgz',
                    'variable': varname,
                    'month': [
                        '01', '02', '03',
                        '04', '05', '06',
                        '07', '08', '09',
                        '10', '11', '12',
                    ],
                    'year': year,
                    'vertical_resolution': vert_res,
                    'product_type': ['consolidated', 'operational']
                },
                path_out
            )
        except Exception as err:
            if varname == 'meridional_velocity':
                ## This is a bugfix due to missing data for this variable
                continue
            else:
                print(varname)
                raise(err)
        with tarfile.open(path_out) as tf:
            tf.extractall(Path(PATH_OUT, 'nc/'))


