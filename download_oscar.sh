#!/bin/bash
for year in {1993..2021}
do
	wget -N https://podaac-opendap.jpl.nasa.gov/opendap/allData/oscar/preview/L4/oscar_third_deg/oscar_vel${year}.nc.gz
done
