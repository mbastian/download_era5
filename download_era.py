import cdsapi
from datetime import datetime, timedelta
#import netCDF4
import os
import time
from tqdm import tqdm

DATASET_2D = 'reanalysis-era5-single-levels-monthly-means'
DATASET_2D_BACK = 'reanalysis-era5-single-levels-monthly-means-preliminary-back-extension'
DATASET_3D = 'reanalysis-era5-pressure-levels-monthly-means'
DATASET_3D_BACK = 'reanalysis-era5-pressure-levels-monthly-means-preliminary-back-extension'

PATH_DIR_OUT = '/home/data-user/data_world_2deg_monthly/'
GRID = [2., 2.]

YEAR_MAX = 2020

#PATH_DIR_OUT = '/home/data-user/data_world_5deg_monthly/'
#GRID = [5.625, 5.625] #latitude/longitude -- default: 0.25 x 0.25

HOURS =[0]#list(range(24)) #[0,6,12,18]

LS_VARIABLE_NAMES_2D = [
        'mean_sea_level_pressure',
        '10m_u_component_of_wind',
        '10m_v_component_of_wind', 
        '2m_temperature', 
        'sea_surface_temperature', 
        'soil_temperature_level_1', 
        'vertical_integral_of_energy_conversion', 
        'vertical_integral_of_kinetic_energy',
        'vertical_integral_of_potential_and_internal_energy',
        'vertical_integral_of_potential_internal_and_latent_energy', 
        'vertical_integral_of_thermal_energy', 
        'vertical_integral_of_total_energy',
        'total_cloud_cover',
        'total_precipitation',
        'clear_sky_direct_solar_radiation_at_surface', 
        'surface_sensible_heat_flux',
        'surface_pressure',
        'sea_ice_cover'
        ]

LS_VARIABLE_NAMES_3D = [
                        'u_component_of_wind', 
                        'v_component_of_wind',
                        'temperature',
                        'specific_humidity',
                        'divergence', 
                        'geopotential', 
                        'potential_vorticity',
                        'vorticity',
                       ]
LS_P_LEVELS = ['1000', '950', '925', '850', '200'][::-1]
LS_PRODUCT_TYPES = ['monthly_averaged_reanalysis', 'monthly_averaged_ensemble_members']
LS_PRODUCT_TYPES_BACK = ['reanalysis-monthly-means-of-daily-means','members-monthly-means-of-daily-means']
os.makedirs(PATH_DIR_OUT, exist_ok=True)

ls_times = []

cds_c = cdsapi.Client()
ls_months = ['{:02d}'.format(month)   for month in range(1, 13)]
ls_days   = ['{:02d}'.format(day)     for day   in range(1, 32)]
ls_hours  = ['{:02d}:00'.format(hour) for hour  in HOURS]

# load constants
cds_c.retrieve(
        DATASET_2D, {
            'variable'      : ['land_sea_mask', 'orography'],
            'product_type'  : 'monthly_averaged_reanalysis',
            'year'          : '2000',
            'month'         : '01',
            'day'           : '01',
            'time'          : '00:00',
            'grid'          : GRID,
            'format'        : 'netcdf' 
        }, os.path.join(PATH_DIR_OUT, "const.nc")
    )
# load  2D variables
for prod_type_n in range(len(LS_PRODUCT_TYPES)): 
    for variable_name in LS_VARIABLE_NAMES_2D:
        print(variable_name)
        for year in tqdm(range(1950, YEAR_MAX+1)):
            if year < 1979:
                dset = DATASET_2D_BACK
                prod_type = LS_PRODUCT_TYPES_BACK[prod_type_n]
            else:
                dset = DATASET_2D
                prod_type = LS_PRODUCT_TYPES[prod_type_n]

            out_path = "{p}/{dset}/{v}_{y}_{prod}.nc".format(
                            dset=dset,
                            p=PATH_DIR_OUT,
                            v=variable_name.lower().replace(' ', '_'), 
                            y=year,
                            prod=prod_type
                            )
            if os.path.isfile(out_path) and (datetime.now() - timedelta(days=180)).year >= year:
                continue
            os.makedirs(os.path.dirname(out_path), exist_ok=True)
            cds_c.retrieve(
                    dset, {
                        'variable'      : variable_name,
                        'product_type'  : prod_type,
                        'year'          : year,
                        'month'         : ls_months,
                        'day'           : ls_days,
                        'grid'          : GRID,
                        'time'          : ls_hours,
                        'format'        : 'netcdf' 
                    }, out_path)


# load  3D variables
for run_nr  in range(1):
    for prod_type_n in range(len(LS_PRODUCT_TYPES)): 
        for level in LS_P_LEVELS:
            for variable_name in LS_VARIABLE_NAMES_3D:
                print(variable_name)
                for year in tqdm(range(1950, YEAR_MAX+1)):
                    if year < 1979:
                        dset = DATASET_3D_BACK
                        prod_type = LS_PRODUCT_TYPES_BACK[prod_type_n]
                    else:
                        dset = DATASET_3D
                        prod_type = LS_PRODUCT_TYPES[prod_type_n]
                    out_path = "{p}/{dset}/{v}:{l}_{y}_{prod}.nc".format(
                                    dset=dset,
                                    p=PATH_DIR_OUT,
                                    v=variable_name.lower().replace(' ', '_'), 
                                    y=year,
                                    l=level,
                                    prod=prod_type
                                    )
                    if os.path.isfile(out_path):
                        continue
                    os.makedirs(os.path.dirname(out_path), exist_ok=True)
                    cds_c.retrieve(
                            dset, {
                                'variable'      : variable_name,
                                'pressure_level': [level],
                                'product_type'  : prod_type,
                                'year'          : year,
                                'month'         : ls_months,
                                'day'           : ls_days,
                                'grid'          : GRID,
                                'time'          : ls_hours,
                                'format'        : 'netcdf' 
                            }, out_path)
